import os
import shutil
import cv2

def imread(img_path):
    if os.path.isfile(img_path):
        shutil.copy(img_path,'temp.jpg')
        img=cv2.imread('temp.jpg')
        return img
    else:
        return -1

img=imread('一.JPG')
cv2.imshow('a',img)
cv2.waitKey()