import torch
import os
import numpy as np
import cv2
import matplotlib.pyplot as plt
from torchvision import datasets, transforms
import torchvision
from PIL import Image
from xml.dom.minidom import parse
import utils
import transforms as T
from engine import train_one_epoch, evaluate
import xml.etree.cElementTree as ET
import collections
import pandas as pd
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor
import sys
import os
import shutil
import tkinter as tk
from tkinter import filedialog


# 这个用来检查是否有空的xml文件


# root = tk.Tk()
# root.withdraw()
# dataset_path = filedialog.askdirectory()
# 把要显示的数据集放在这个文件夹里
root = r'dataset'
train_root = os.path.join(root, 'train')
# root = r'dataset\train'
img_path = os.path.join(train_root, r'JPEGImages')
xml_path = os.path.join(train_root, r'Annotations')
img_list = os.listdir(img_path)
xml_list = os.listdir(xml_path)

print(img_list)
print(xml_list)

# 解析label_list文件
with open(os.path.join(root, "label_list.txt"), 'r') as file:
    label_list = file.readlines()
label_list = [label.rstrip() for label in label_list]  # 去掉空字符
label_list = [label for label in label_list if label != '']  # 去掉空行

for i in range(len(xml_list)):
    bbox_xml_path = os.path.join(train_root, 'Annotations', xml_list[i])
    dom = parse(bbox_xml_path)
    data = dom.documentElement
    objects = data.getElementsByTagName('object')
    labels = []
    for object_ in objects:
        # name就是label字符串
        name = object_.getElementsByTagName('name')[0].childNodes[0].nodeValue  # 就是label
        if name not in label_list:
            print(xml_list[i])
            print('wrong label:', str(name))
        # 返回的应该是一个列表，但是这里只有一个bndbox，但是仍然要用下标0来获得第一个的对象
        bndbox = object_.getElementsByTagName('bndbox')[0]
        xmin = np.float(bndbox.getElementsByTagName('xmin')[0].childNodes[0].nodeValue)
        ymin = np.float(bndbox.getElementsByTagName('ymin')[0].childNodes[0].nodeValue)
        xmax = np.float(bndbox.getElementsByTagName('xmax')[0].childNodes[0].nodeValue)
        ymax = np.float(bndbox.getElementsByTagName('ymax')[0].childNodes[0].nodeValue)
        # 列表汇总，附加一个四个数的列表
        # boxes.append([xmin, ymin, xmax, ymax])
    if not objects:
        print(xml_list[i])
        print('empty label')
