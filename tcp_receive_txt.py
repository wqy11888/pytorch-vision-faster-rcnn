import socket
import os
import shutil
import cv2


# 终止标志位
FLAG = b'$END$'


# 要求地址，是IP+端口的形式
# 这里，BUF是1024
def recv_txt_TCP(addr, BUF=1024):
    sk = socket.socket()  # socket对象
    # print('.')
    sk.bind(addr)  # 绑定套接字到本机的网络地址
    sk.listen(10)  # 10表示最大连接数
    client, cli_addr = sk.accept()  # 接受，这里会卡住不往下运行，直到接收到数据
    data = client.recv(1024)
    data = data.decode()
    return data


if __name__ == "__main__":
    addr = ('192.168.1.53', 20020)    # 本机的ip地址和端口号
    boxes = recv_txt_TCP(addr)



