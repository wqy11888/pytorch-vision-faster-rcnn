from aip import AipFace
import base64
import cv2
import os
import time


""" 你的 APPID AK SK """
APP_ID = '22912073'
API_KEY = '3zbPMNiqWOrsD5BspgX1pBoR'
SECRET_KEY = 'nxSoiCZnSdOV9DVTPhHrlMvAYvTHZNa4'

# aip_face对象
aipFace = AipFace(APP_ID, API_KEY, SECRET_KEY)

# 读取图片，转base64
# filepath = r"face_test\1.jpg"
# with open(filepath, "rb") as fp:
#     base64_data = base64.b64encode(fp.read())
# image = str(base64_data, 'utf-8')
# imageType = "BASE64"

# 读取所有图片
face_root = r'face'
face_imgs = os.listdir(face_root)
print(face_imgs)

for face_img in face_imgs:
    path = os.path.join(face_root, face_img)
    with open(path, "rb") as fp:
        base64_data = base64.b64encode(fp.read())
    image = str(base64_data, 'utf-8')
    imageType = "BASE64"

    # 准备注册用信息
    group_Id = 'main'
    userId = face_img.split()[1]
    user_info = face_img.split()[1] + face_img.split()[2]   # 将整个文件名作为information放在里面
    print(group_Id)
    print(userId)

    # 配置人脸注册option
    options = {}
    options['user_info'] = user_info
    options['quality_control'] = "NONE"
    options["liveness_control"] = "NONE"
    # options["action_type"] = "APPEND"
    options["action_type"] = "REPLACE"

    aipFace.addUser(image, imageType, group_Id, userId, options)

    time.sleep(0.5)
