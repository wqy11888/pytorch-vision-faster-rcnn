import os
import sys
import shutil


# 创建目录
if 'dataset' not in os.listdir():
    os.mkdir('dataset')
    os.mkdir(os.path.join('dataset', 'train'))
    os.mkdir(os.path.join('dataset', 'validation'))
    os.mkdir(os.path.join('dataset', 'test'))
    os.mkdir(os.path.join('dataset', 'models'))
    os.mkdir(os.path.join('dataset', 'prediction'))
    os.mkdir(os.path.join('dataset', 'temp'))
    src = r'label_list_all.txt'
    dst = os.path.join('dataset', 'label_list.txt')
    shutil.copy(src, dst)
    print('created')
else:
    print('exits')

print('done')
# os.mkdir('dataset')
