import os
import socket

FLAG='$END$'
def sendFile_TCP(FILEPATH, ADDR, BUF=1024*64):
    sk = socket.socket()            # 初始化套接字
    sk.connect(ADDR)                # 尝试连接目标主机，如果失败会引发异常，这里为了简便没有进行异常处理
    sum = 0
    sp = '\\'
    if sp not in FILEPATH:
        sp = '/'
    filename = FILEPATH.split(sp)[-1]   # 分割路径字符串,获取文件名
    sk.send(filename.encode('utf-8'))
    with open(FILEPATH,'rb') as f:
        while True:
            os.system('title 正在发送文件:{}  已发送 {} KB'.format(filename,int(sum/1024)))
            data = f.read(BUF)
            if not data:
                break
            sk.send(data)
            sum += len(data)
    sk.send(FLAG.encode())
    os.system('title FileSender')
    sk.close()  # 关闭套接字


if __name__ == "__main__":
    addr = ('192.168.1.107', 20020)  # 目的地的IP和端口，端口不要动
    while True:
        print('【拖拽文件到此处】')
        path = input()
        sendFile_TCP(path, addr)
