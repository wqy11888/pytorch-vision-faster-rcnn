# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

# 导入CV2模块
import cv2
import os
import time

print("=============================================")
print("=  热键(请在摄像头的窗口使用)：             =")
print("=  z: 更改存储目录                          =")
print("=  x: 拍摄图片                              =")
print("=  q: 退出                                  =")
print("=============================================")
#提醒用户操作字典

#class_name = input("请输入存储目录：") 有问题 会使程序卡死 我太菜了不会python不知道怎么改
"""
while os.path.exists(class_name):
    class_name = input("目录已存在！请输入存储目录：")
os.mkdir(class_name)
"""



def read_usb_capture():
    # 选择摄像头的编号
    index = 0
    cap = cv2.VideoCapture(0)
    # 添加这句是可以用鼠标拖动弹出的窗体
    cv2.namedWindow('real_img', cv2.WINDOW_NORMAL)

    flag=0
    while(cap.isOpened()):
        time.sleep(0.05)
        # 读取摄像头的画面
        ret, frame = cap.read()
        # 真实图
        cv2.imshow('real_img', frame)



        input = cv2.waitKey(1) & 0xFF

        #更改存储路径(似乎有问题)
        """
        if input == ord('z'):
            class_name = input("请输入存储目录：")
            while os.path.exists(class_name):
                class_name = input("目录已存在！请输入存储目录：")
            os.mkdir(class_name)
        """
        # 按下"x"存储

        if input == ord('x'):
            flag=1

        if flag:
            cv2.imwrite("%s/%d.jpg" % ("D:/images/tape", index),
            cv2.resize(frame, (1080, 720), interpolation=cv2.INTER_AREA))
            print("%s: %d 张图片" % ("D:/images/tape", index))
            index += 1


        # 按下'q'就退出
        if input == ord('q'):
            break
    # 释放画面
    cap.release()
    cv2.destroyAllWindows()


if __name__ == '__main__':
    read_usb_capture()
