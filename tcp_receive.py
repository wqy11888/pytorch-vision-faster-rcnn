import socket
import os
import shutil
import cv2


# 终止标志位
FLAG = b'$END$'


# 要求地址，是IP+端口的形式
# 这里，BUF是1024
def recvFile_TCP(ADDR, BUF=1024):
    sk = socket.socket()  # socket对象
    # print('.')
    sk.bind(ADDR)  # 绑定套接字到本机的网络地址
    sk.listen(10)  # 10表示最大连接数
    while True:
        client, cli_addr = sk.accept()      # 接受连接请求，返回了一个客户端对象和地址
        filename = client.recv(BUF).decode('utf-8')  # 调用recv函数，来接受一个BUF的数据
        # print('正在接受来自 [{}] 的文件[{}]'.format(cli_addr, filename))
        print('detecting...')
        i = 0
        while os.path.exists(filename):
            filename = '{}-{}'.format(i, filename)
            i += 1
        # 新建文件，并打开
        with open(filename, 'ab') as f:
            while True:
                # 接受一个BUF的数据
                data = client.recv(BUF)
                # 直到终止地址
                if data == FLAG:
                    break
                f.write(data)
        # print('已成功接收文件{}  |  文件大小 : {} KB'.format(filename,int(os.path.getsize(filename) / 1024)))
        print('done')
        client.close()
        return filename


if __name__ == "__main__":
    addr = ('192.168.1.53', 20020)    #本机的ip地址和端口号
    file_name = recvFile_TCP(addr)
    src = file_name
    dst = os.path.join('prediction', file_name)
    shutil.copy(src, dst)
    img_path = dst
    img = cv2.imread(img_path)
    cv2.imshow('detection', img)
    cv2.waitKey()
    cv2.destroyAllWindows()



