import os
import shutil

def Fix(xml_path,old_name,new_name):
    os.mkdir('xml_temp')
    for root, dirs, files in os.walk(xml_path):
        for file in files:
            fi = open(xml_path + '/' + file, 'r')
            fo = open('xml_temp/' + file, 'w')
            content = fi.readlines()
            for line in content:
                line = line.replace(old_name, new_name)
                fo.write(line)
        break
    fi.close()
    fo.close()
    shutil.rmtree(xml_path)
    os.replace('xml_temp',xml_path)

Fix('D:/images/dataset_origin2/Annotations','cannned fruit','canned fruit')
